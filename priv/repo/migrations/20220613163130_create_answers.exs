defmodule Torstai.Repo.Migrations.CreateAnswers do
  use Ecto.Migration

  def change do
    create table(:answers) do
      add :answer, :string
      add :user_session, :string
      add :question_id, references(:questions, on_delete: :nothing)

      timestamps()
    end

    create index(:answers, [:question_id])
    create index(:answers, [:user_session])
    create unique_index(:answers, [:user_session, :question_id], name: :index_users_on_question)
  end
end
