defmodule Torstai.Repo.Migrations.CreateSurveys do
  use Ecto.Migration

  def change do
    create table(:surveys) do
      add :name, :string
      add :code, :string

      timestamps()
    end

    create unique_index(:surveys, [:code])
  end
end
