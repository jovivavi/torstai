defmodule Torstai.Repo do
  use Ecto.Repo,
    otp_app: :torstai,
    adapter: Ecto.Adapters.Postgres
end
