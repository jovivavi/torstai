defmodule Torstai.Surveys.SurveySearch do
  defstruct [:code]
  @types %{code: :string}

  import Ecto.Changeset

  def changeset(%__MODULE__{} = search, attrs) do
    {search, @types}
    |> cast(attrs, Map.keys(@types))
    |> validate_required([:code])
  end
end
