defmodule Torstai.Surveys.Answer do
  use Ecto.Schema
  import Ecto.Changeset

  schema "answers" do
    field :answer, :string
    field :user_session, :string
    belongs_to :question, Torstai.Surveys.Question

    timestamps()
  end

  @doc false
  def changeset(answer, attrs) do
    answer
    |> cast(attrs, [:answer, :user_session, :question_id])
    |> validate_required([:answer, :user_session, :question_id])
    |> validate_inclusion(:answer, ["yes", "no"])
    |> unique_constraint(:user_session, name: :index_users_on_question)
  end
end
