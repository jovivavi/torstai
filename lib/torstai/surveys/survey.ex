defmodule Torstai.Surveys.Survey do
  use Ecto.Schema
  import Ecto.Changeset

  schema "surveys" do
    field :code, :string
    field :name, :string
    has_many :questions, Torstai.Surveys.Question

    timestamps()
  end

  @doc false
  def changeset(survey, attrs) do
    survey
    |> cast(attrs, [:name, :code])
    |> validate_required([:name, :code])
  end
end
