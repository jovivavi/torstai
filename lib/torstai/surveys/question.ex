defmodule Torstai.Surveys.Question do
  use Ecto.Schema
  import Ecto.Changeset

  schema "questions" do
    field :question, :string
    field :answered, :boolean, virtual: true, default: false
    belongs_to :survey, Torstai.Surveys.Survey
    has_many :answer, Torstai.Surveys.Answer

    timestamps()
  end

  @doc false
  def changeset(question, attrs) do
    question
    |> cast(attrs, [:question, :survey_id])
    |> validate_required([:question, :survey_id])
    |> validate_format(:question, ~r/\?/)
  end
end
