defmodule TorstaiWeb.SurveyLive.Questions do
  use TorstaiWeb, :live_component
  alias Torstai.Surveys

  def update(assigns, socket) do
    {:ok,
     socket
     |> assign(assigns)
     |> assign_questions()}
  end

  def assign_questions(%{assigns: %{id: survey_id}} = socket) do
    q = Surveys.get_survey_questions(survey_id)
    assign(socket, :questions, q)
  end

  def render(assigns) do
    ~H"""
    <div>
      <%= for question <- @questions do %>
        <li>
          <%= question.question %> Yes: <%= count_yes_answers(question) %> No: <%= count_no_answers(
            question
          ) %>
        </li>
      <% end %>
    </div>
    """
  end

  def count_yes_answers(question) do
    question.answer
    |> Enum.filter(fn q ->
      q.answer == "yes"
    end)
    |> Enum.count()
  end

  def count_no_answers(question) do
    question.answer
    |> Enum.filter(fn q ->
      q.answer == "no"
    end)
    |> Enum.count()
  end
end
