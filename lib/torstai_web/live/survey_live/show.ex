defmodule TorstaiWeb.SurveyLive.Show do
  use TorstaiWeb, :live_view

  alias Torstai.Surveys
  alias TorstaiWeb.Endpoint
  @answer_topic "answer"

  @impl true
  def mount(_params, _session, socket) do
    if connected?(socket) do
      Endpoint.subscribe(@answer_topic)
    end

    {:ok, socket}
  end

  @impl true
  def handle_info(%{event: "answer_created"}, socket) do
    send_update(TorstaiWeb.SurveyLive.Questions, id: socket.assigns.survey.id)
    {:noreply, socket}
  end

  @impl true
  def handle_params(%{"id" => id}, _, socket) do
    {:noreply,
     socket
     |> assign(:page_title, page_title(socket.assigns.live_action))
     |> assign(:survey, Surveys.get_survey!(id))}
  end

  defp page_title(:show), do: "Show Survey"
  defp page_title(:edit), do: "Edit Survey"
  defp page_title(:question), do: "Add question"
end
