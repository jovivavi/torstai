defmodule TorstaiWeb.HelloNoAuthLive do
  use TorstaiWeb, :live_view

  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  def render(assigns) do
    ~H"""
    <h1>Hello no auth</h1>
    """
  end
end
