defmodule TorstaiWeb.NoAuthSessionLive do
  import Phoenix.LiveView

  def on_mount(:default, _params, _session, socket) do
    s = for _ <- 1..10, into: "", do: <<Enum.random('0123456789abcdef')>>

    {:cont,
     socket
     |> assign_new(:user_session, fn -> s end)}
  end
end
