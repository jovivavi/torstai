defmodule TorstaiWeb.AnswerLive.Search do
  use TorstaiWeb, :live_component

  alias Torstai.Surveys

  def update(_assigns, socket) do
    {:ok,
     socket
     |> assign_changeset()}
  end

  def handle_event("save", %{"survey_search" => %{"code" => code}}, socket) do
    survey = Surveys.get_survey_by_code(code)

    if survey do
      send(self(), {:found_survey, survey})
    end

    {:noreply, socket}
  end

  defp assign_changeset(socket) do
    assign(socket, :changeset, Surveys.change_survey_search(%Surveys.SurveySearch{}))
  end
end
