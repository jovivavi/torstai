defmodule TorstaiWeb.AnswerLive.Show do
  use TorstaiWeb, :live_view

  alias Torstai.Surveys
  alias TorstaiWeb.Endpoint
  @answer_topic "answer"
  @question_topic "question"

  def mount(_params, _session, socket) do
    if connected?(socket) do
      Endpoint.subscribe(@question_topic)
    end

    {:ok, socket}
  end

  def handle_params(%{"id" => id}, _session, socket) do
    {:noreply,
     socket
     |> assign(:survey_id, id)
     |> assign_questions()}
  end

  def assign_questions(%{assigns: %{survey_id: survey_id}} = socket) do
    questions = Surveys.get_survey_questions(survey_id)

    socket
    |> assign(:questions, questions)
  end

  def handle_event("answer", %{"answer" => answer, "qid" => qid}, socket) do
    answer_params = %{
      question_id: qid,
      answer: answer,
      user_session: socket.assigns.user_session
    }

    case Surveys.create_answer(answer_params) do
      {:ok, answer} ->
        Endpoint.broadcast(@answer_topic, "answer_created", socket.assigns.survey_id)

        answered_question =
          Enum.find(socket.assigns.questions, fn q -> q.id == answer.question_id end)

        {:noreply,
         socket
         |> assign_answered_question(answer)
         |> put_flash(:info, "Question \"#{answered_question.question}\" answered!")}

      {:error, _changeset} ->
        {:noreply, socket}
    end
  end

  defp assign_answered_question(%{assigns: %{questions: questions}} = socket, answer) do
    mapped_questions =
      Enum.map(questions, fn q ->
        if q.id == answer.question_id do
          %Surveys.Question{q | answered: true}
        else
          q
        end
      end)

    socket
    |> assign(:questions, mapped_questions)
  end

  def handle_info(%{event: "question_created"}, socket) do
    # questions = Surveys.get_survey_questions(socket.assigns.survey_id)
    questions =
      Surveys.get_survey_questions(socket.assigns.survey_id)
      |> Enum.map(fn q ->
        %Surveys.Question{
          q
          | answered:
              Enum.any?(q.answer, fn a -> a.user_session == socket.assigns.user_session end)
        }
      end)

    {:noreply,
     socket
     |> assign(:questions, questions)}
  end

  def question(assigns) do
    ~H"""
    <div>
      <%= @question.question %> <br />
      <button
        phx-click="answer"
        phx-value-answer="yes"
        disabled={@question.answered}
        phx-value-qid={@question.id}
      >
        Yes
      </button>
      <button
        phx-click="answer"
        phx-value-answer="no"
        disabled={@question.answered}
        phx-value-qid={@question.id}
      >
        No
      </button>
    </div>
    """
  end
end
