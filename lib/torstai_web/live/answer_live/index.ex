defmodule TorstaiWeb.AnswerLive.Index do
  use TorstaiWeb, :live_view

  def handle_info({:found_survey, survey}, socket) do
    {:noreply,
     socket
     |> push_redirect(to: Routes.answer_show_path(socket, :show, survey))}
  end
end
