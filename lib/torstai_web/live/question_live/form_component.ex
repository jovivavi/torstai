defmodule TorstaiWeb.QuestionLive.FormComponent do
  use TorstaiWeb, :live_component
  alias Torstai.Surveys
  alias TorstaiWeb.Endpoint
  @question_topic "question"

  @impl true
  def update(assigns, socket) do
    changeset = Surveys.change_question(%Surveys.Question{})

    {:ok,
     socket
     |> assign(assigns)
     |> assign(:changeset, changeset)}
  end

  @impl true
  def handle_event("validate", %{"question" => question_params}, socket) do
    changeset =
      socket.assigns.question
      |> Surveys.change_question(question_params)
      |> Map.put(:action, :validate)

    {:noreply, assign(socket, :changeset, changeset)}
  end

  def handle_event("save", %{"question" => question_params}, socket) do
    case Surveys.create_question(question_params) do
      {:ok, _question} ->
        Endpoint.broadcast(@question_topic, "question_created", %{})

        {:noreply,
         socket
         |> put_flash(:info, "Question created successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, changeset: changeset)}
    end
  end
end
