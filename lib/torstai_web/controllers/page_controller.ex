defmodule TorstaiWeb.PageController do
  use TorstaiWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
