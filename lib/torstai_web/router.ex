defmodule TorstaiWeb.Router do
  use TorstaiWeb, :router

  import TorstaiWeb.UserAuth
  import Phoenix.LiveDashboard.Router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {TorstaiWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug :fetch_current_user
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", TorstaiWeb do
    pipe_through :browser

    get "/", PageController, :index
  end

  # Other scopes may use custom stacks.
  # scope "/api", TorstaiWeb do
  #   pipe_through :api
  # end

  # Enables the Swoosh mailbox preview in development.
  #
  # Note that preview only shows emails that were sent by the same
  # node running the Phoenix server.
  if Mix.env() == :dev do
    scope "/dev" do
      pipe_through :browser

      forward "/mailbox", Plug.Swoosh.MailboxPreview
    end
  end

  ## Authentication routes

  scope "/", TorstaiWeb do
    pipe_through [:browser, :redirect_if_user_is_authenticated]

    get "/users/register", UserRegistrationController, :new
    post "/users/register", UserRegistrationController, :create
    get "/users/log_in", UserSessionController, :new
    post "/users/log_in", UserSessionController, :create
    get "/users/reset_password", UserResetPasswordController, :new
    post "/users/reset_password", UserResetPasswordController, :create
    get "/users/reset_password/:token", UserResetPasswordController, :edit
    put "/users/reset_password/:token", UserResetPasswordController, :update
  end

  scope "/", TorstaiWeb do
    pipe_through [:browser, :require_authenticated_user]

    live_dashboard "/dashboard", metrics: TorstaiWeb.Telemetry

    live_session :default, on_mount: TorstaiWeb.UserLiveAuth do
      live "/hello", HelloLive

      live "/surveys", SurveyLive.Index, :index
      live "/surveys/new", SurveyLive.Index, :new
      live "/surveys/:id/edit", SurveyLive.Index, :edit

      live "/surveys/:id", SurveyLive.Show, :show
      live "/surveys/:id/show/edit", SurveyLive.Show, :edit
      live "/surveys/:id/show/add-question", SurveyLive.Show, :question
    end

    get "/users/settings", UserSettingsController, :edit
    put "/users/settings", UserSettingsController, :update
    get "/users/settings/confirm_email/:token", UserSettingsController, :confirm_email
  end

  scope "/", TorstaiWeb do
    pipe_through [:browser]

    live_session :no_auth, on_mount: TorstaiWeb.NoAuthSessionLive do
      live "/hello-no-auth", HelloNoAuthLive
      live "/answer", AnswerLive.Index, :index
      live "/answer/:id", AnswerLive.Show, :show
    end

    delete "/users/log_out", UserSessionController, :delete
    get "/users/confirm", UserConfirmationController, :new
    post "/users/confirm", UserConfirmationController, :create
    get "/users/confirm/:token", UserConfirmationController, :edit
    post "/users/confirm/:token", UserConfirmationController, :update
  end
end
