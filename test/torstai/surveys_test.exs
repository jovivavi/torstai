defmodule Torstai.SurveysTest do
  use Torstai.DataCase

  alias Torstai.Surveys

  describe "surveys" do
    alias Torstai.Surveys.Survey

    import Torstai.SurveysFixtures

    @invalid_attrs %{code: nil, name: nil}

    test "list_surveys/0 returns all surveys" do
      survey = survey_fixture()
      assert Surveys.list_surveys() == [survey]
    end

    test "get_survey!/1 returns the survey with given id" do
      survey = survey_fixture()
      assert Surveys.get_survey!(survey.id) == survey
    end

    test "create_survey/1 with valid data creates a survey" do
      valid_attrs = %{code: "some code", name: "some name"}

      assert {:ok, %Survey{} = survey} = Surveys.create_survey(valid_attrs)
      assert survey.code == "some code"
      assert survey.name == "some name"
    end

    test "create_survey/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Surveys.create_survey(@invalid_attrs)
    end

    test "update_survey/2 with valid data updates the survey" do
      survey = survey_fixture()
      update_attrs = %{code: "some updated code", name: "some updated name"}

      assert {:ok, %Survey{} = survey} = Surveys.update_survey(survey, update_attrs)
      assert survey.code == "some updated code"
      assert survey.name == "some updated name"
    end

    test "update_survey/2 with invalid data returns error changeset" do
      survey = survey_fixture()
      assert {:error, %Ecto.Changeset{}} = Surveys.update_survey(survey, @invalid_attrs)
      assert survey == Surveys.get_survey!(survey.id)
    end

    test "delete_survey/1 deletes the survey" do
      survey = survey_fixture()
      assert {:ok, %Survey{}} = Surveys.delete_survey(survey)
      assert_raise Ecto.NoResultsError, fn -> Surveys.get_survey!(survey.id) end
    end

    test "change_survey/1 returns a survey changeset" do
      survey = survey_fixture()
      assert %Ecto.Changeset{} = Surveys.change_survey(survey)
    end
  end

  describe "questions" do
    alias Torstai.Surveys.Question

    import Torstai.SurveysFixtures

    @invalid_attrs %{answer: nil, question: nil}

    test "list_questions/0 returns all questions" do
      question = question_fixture()
      assert Surveys.list_questions() == [question]
    end

    test "get_question!/1 returns the question with given id" do
      question = question_fixture()
      assert Surveys.get_question!(question.id) == question
    end

    test "create_question/1 with valid data creates a question" do
      valid_attrs = %{answer: true, question: "some question"}

      assert {:ok, %Question{} = question} = Surveys.create_question(valid_attrs)
      assert question.answer == true
      assert question.question == "some question"
    end

    test "create_question/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Surveys.create_question(@invalid_attrs)
    end

    test "update_question/2 with valid data updates the question" do
      question = question_fixture()
      update_attrs = %{answer: false, question: "some updated question"}

      assert {:ok, %Question{} = question} = Surveys.update_question(question, update_attrs)
      assert question.answer == false
      assert question.question == "some updated question"
    end

    test "update_question/2 with invalid data returns error changeset" do
      question = question_fixture()
      assert {:error, %Ecto.Changeset{}} = Surveys.update_question(question, @invalid_attrs)
      assert question == Surveys.get_question!(question.id)
    end

    test "delete_question/1 deletes the question" do
      question = question_fixture()
      assert {:ok, %Question{}} = Surveys.delete_question(question)
      assert_raise Ecto.NoResultsError, fn -> Surveys.get_question!(question.id) end
    end

    test "change_question/1 returns a question changeset" do
      question = question_fixture()
      assert %Ecto.Changeset{} = Surveys.change_question(question)
    end
  end

  describe "answers" do
    alias Torstai.Surveys.Answer

    import Torstai.SurveysFixtures

    @invalid_attrs %{answer: nil, user_session: nil}

    test "list_answers/0 returns all answers" do
      answer = answer_fixture()
      assert Surveys.list_answers() == [answer]
    end

    test "get_answer!/1 returns the answer with given id" do
      answer = answer_fixture()
      assert Surveys.get_answer!(answer.id) == answer
    end

    test "create_answer/1 with valid data creates a answer" do
      valid_attrs = %{answer: "some answer", user_session: "some user_session"}

      assert {:ok, %Answer{} = answer} = Surveys.create_answer(valid_attrs)
      assert answer.answer == "some answer"
      assert answer.user_session == "some user_session"
    end

    test "create_answer/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Surveys.create_answer(@invalid_attrs)
    end

    test "update_answer/2 with valid data updates the answer" do
      answer = answer_fixture()
      update_attrs = %{answer: "some updated answer", user_session: "some updated user_session"}

      assert {:ok, %Answer{} = answer} = Surveys.update_answer(answer, update_attrs)
      assert answer.answer == "some updated answer"
      assert answer.user_session == "some updated user_session"
    end

    test "update_answer/2 with invalid data returns error changeset" do
      answer = answer_fixture()
      assert {:error, %Ecto.Changeset{}} = Surveys.update_answer(answer, @invalid_attrs)
      assert answer == Surveys.get_answer!(answer.id)
    end

    test "delete_answer/1 deletes the answer" do
      answer = answer_fixture()
      assert {:ok, %Answer{}} = Surveys.delete_answer(answer)
      assert_raise Ecto.NoResultsError, fn -> Surveys.get_answer!(answer.id) end
    end

    test "change_answer/1 returns a answer changeset" do
      answer = answer_fixture()
      assert %Ecto.Changeset{} = Surveys.change_answer(answer)
    end
  end
end
