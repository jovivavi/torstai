defmodule Torstai.SurveysFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Torstai.Surveys` context.
  """

  @doc """
  Generate a survey.
  """
  def survey_fixture(attrs \\ %{}) do
    {:ok, survey} =
      attrs
      |> Enum.into(%{
        code: "some code",
        name: "some name"
      })
      |> Torstai.Surveys.create_survey()

    survey
  end

  @doc """
  Generate a question.
  """
  def question_fixture(attrs \\ %{}) do
    {:ok, question} =
      attrs
      |> Enum.into(%{
        answer: true,
        question: "some question"
      })
      |> Torstai.Surveys.create_question()

    question
  end

  @doc """
  Generate a answer.
  """
  def answer_fixture(attrs \\ %{}) do
    {:ok, answer} =
      attrs
      |> Enum.into(%{
        answer: "some answer",
        user_session: "some user_session"
      })
      |> Torstai.Surveys.create_answer()

    answer
  end
end
